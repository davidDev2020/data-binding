package com.example.databinding.simple;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.databinding.R;
import com.example.databinding.databinding.ActivitySimpleBinding;
import com.example.databinding.model.Product;
import com.example.databinding.model.ProductProvider;

public class SimpleActivity extends AppCompatActivity {

    //data binding
    ActivitySimpleBinding  mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //delete set content view and add this line
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_simple);
        Product product =  new Product();
        product.setTitle("james");
//        mBinding.setProductsssss(ProductProvider.RED_LAMP);
        mBinding.setProductsssss(product);
        product.setTitle("James2");

    }
}
