package com.example.databinding;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.databinding.fragment.ActivityWithFragment;
import com.example.databinding.simple.SimpleActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void simpleBnding(View view) {
        startActivity(new Intent(this, SimpleActivity.class));
    }

    public void bindingWithFragment(View view) {
        startActivity(new Intent(this, ActivityWithFragment.class));
    }
}
