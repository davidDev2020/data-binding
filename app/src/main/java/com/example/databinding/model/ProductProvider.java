package com.example.databinding.model;

import com.example.databinding.R;

import java.math.BigDecimal;

public class ProductProvider {

    public static final Product RED_LAMP = new Product("Red Lamp", "Red colored lamp, perfect for lighting up a room " +
            "and matching any red furniture.", R.drawable.red_lamp, new BigDecimal(10.99), new BigDecimal(9.50), 161,
            new BigDecimal(4.5), 1515611);
}
