package com.example.databinding.fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.databinding.R;
import com.example.databinding.databinding.ActivityWithFragmentBinding;
import com.example.databinding.fragment.ViewProductFragment;

public class ActivityWithFragment extends AppCompatActivity {


    ActivityWithFragmentBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_with_fragment);
        init();
    }


    void init(){

        ViewProductFragment fragment = ViewProductFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayoutID,fragment,"abc");
        transaction.commit();
    }


}
