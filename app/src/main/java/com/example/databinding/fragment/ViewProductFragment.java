package com.example.databinding.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.databinding.databinding.FragmentViewProductBinding;
import com.example.databinding.model.Product;
import com.example.databinding.model.ProductProvider;


public class ViewProductFragment extends Fragment {
    FragmentViewProductBinding mBinding ;


    public ViewProductFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ViewProductFragment newInstance() {
        ViewProductFragment fragment = new ViewProductFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentViewProductBinding.inflate(inflater);
        Product product = ProductProvider.RED_LAMP;
        mBinding.setProduct(product);
        mBinding.setIsEnable(false);
        return mBinding.getRoot();
    }

}
